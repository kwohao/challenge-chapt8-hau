import "../../index.css";
import { Link } from "react-router-dom";

const AuthLayout = (props) => {
  const { children, title, type } = props;
  return (
    <div className="mainBg cardPosition flex justify-center h-full items-center">
      <div className=" border-8 border-blue-500 p-10 rounded-xl bg-gray-200">
        <div className="w-full max-w-sm ">
          <h1 className="text-blue-600 font-bold text-3xl mb-1">{title}</h1>
          <p className="text-gray-400 font-semibold mb-7">
            Welcome, Please enter your credentials
          </p>
          {children}
          <p className="text-center text-sm text-gray-700 mt-3">
            {type === "login"
              ? "Don't have an account "
              : "Already have an account? "}

            {type === "login" && (
              <Link to="/register" className="text-blue-600 font-bold">
                Register
              </Link>
            )}

            {type === "register" && (
              <Link to="/login" className="text-blue-600 font-bold">
                Login
              </Link>
            )}
          </p>
        </div>
      </div>
    </div>
  );
};
export default AuthLayout;
