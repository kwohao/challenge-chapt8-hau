const Button = (props) => {
  const { variant = "bg-black", children = "...", onClick } = props;
  return (
    <button
      className={`h-10 px-6 font-semibold rounded-md ${variant} text-white`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
