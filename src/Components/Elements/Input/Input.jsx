const Input = (props) => {
  const { type, placeholder, name, className, onChange } = props;
  return (
    <input
      type={type}
      // className="text-sm w-full border rounded placeholder-black opacity-50 p-2"
      className={`text-sm w-full border rounded placeholder-black opacity-50 p-2 ${className}`}
      placeholder={placeholder}
      name={name}
      id={name}
      onChange={onChange}
    />
  );
};

export default Input;
