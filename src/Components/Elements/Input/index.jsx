import Label from "./Label";
import Input from "./Input";

const inputForm = (props) => {
  const { label, name, type, placeholder, variant, className, onChange } =
    props;
  return (
    <div className="mb-5">
      <Label htmlFor={name} variant={variant}>
        {label}
      </Label>
      <Input
        type={type}
        placeholder={placeholder}
        name={name}
        className={className}
        onChange={onChange}
      />
    </div>
  );
};

export default inputForm;
