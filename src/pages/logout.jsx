import { Navigate } from "react-router-dom";

const Logout = () => {
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    localStorage.removeItem("accessToken");
  } else {
    if (!accessToken) {
      return <Navigate to="/login" />;
    }
  }
};

export default Logout;
