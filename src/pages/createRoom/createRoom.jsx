import "./createRoom.css";
import Button from "../../Components/Elements/Button/Button";
import InputForm from "../../Components/Elements/Input";
import RpsImage from "../../Components/Fragments/RpsImage/RpsImage";
import { useState } from "react";

const CreateRoom = () => {
  const [userChoice, setUserChoice] = useState("");
  return (
    <div className="flex w-full h-screen bg-gray-700">
      <div className="w-1/3">
        <div className="flex justify-center pilihan">
          <RpsImage
            src="./assets/images/batu.png"
            id="batu"
            classname={userChoice === "batu" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (userChoice === "") {
                setUserChoice("batu");
              }
            }}
          />
        </div>
      </div>
      <div className="w-1/3">
        <form action="">
          <InputForm
            name="roomname"
            type="text"
            placeholder="Insert Room Name Here.."
            className="h-18 text-xl text-center"
            variant="text-orange-500 text-center text-3xl mt-10 mb-5"
          />
        </form>
        <div className="flex justify-center gunting">
          <RpsImage
            src="./assets/images/gunting.png"
            id="gunting"
            classname={userChoice === "gunting" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (userChoice === "") {
                setUserChoice("gunting");
              }
            }}
          />
        </div>
        <Button variant="bg-orange-500 tombolPilihan w-60 h-20 text-3xl hover:bg-orange-600 hover:scale-110 hover:cursor-pointer transition-all">
          Choose
        </Button>
      </div>
      <div className="w-1/3 ">
        <div className="flex justify-center pilihan">
          <RpsImage
            src="./assets/images/kertas.png"
            id="kertas"
            classname={userChoice === "kertas" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (userChoice === "") {
                setUserChoice("kertas");
              }
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default CreateRoom;
