// import React from "react";
import { Link, Outlet } from "react-router-dom";
// import Logout from "./logout";

const Navbar = () => {
  return (
    <div className="flex flex-col w-full h-screen">
      <nav className="flex p-5 text-white bg-slate-500 justify-between items-center cursor-pointer">
        <div className="text-gray-200 hover:scale-110 hover:cursor-pointer transition-all hover:bg-black p-1 px-2 rounded">
          <Link to={"/all-rooms"}>
            <span className="text-2xl font-bold">Rock Paper Scissors</span>
          </Link>
        </div>
        <ul className="flex items-center justify-between gap-10">
          <li className="hover:scale-110 hover:cursor-pointer transition-all hover:text-white hover:bg-black p-1 px-2 rounded">
            <Link to={"/login"} className="text-xl duratin-500">
              Login
            </Link>
          </li>
          <li className="hover:scale-110 hover:cursor-pointer transition-all hover:text-white hover:bg-black p-1 px-2 rounded">
            <Link to={"/register"} className="text-xl duratin-500">
              Register
            </Link>
          </li>
          <li className="hover:scale-110 hover:cursor-pointer transition-all hover:text-white hover:bg-black p-1 px-2 rounded">
            <Link to={"/all-rooms"} className="text-xl duratin-500">
              Room List
            </Link>
          </li>
          {/* <li className="hover:scale-110 hover:cursor-pointer transition-all">
            <Link
              to={"/login"}
              className="text-xl hover:text-black duratin-500"
            >
              Logout
              <Logout></Logout>
            </Link>
          </li> */}
        </ul>
      </nav>
      <Outlet />
    </div>
  );
};

export default Navbar;
