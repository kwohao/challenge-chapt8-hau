import Button from "../Components/Elements/Button/Button";
import FormBiodata from "../Components/Fragments/FormBiodata";
import { Link } from "react-router-dom";
import { useState } from "react";

const AllRoom = () => {
  const [rooms] = useState([
    {
      roomName: "Room One",
      player1: "Naryo",
      judgement: "WIN",
      status: "Finished",
    },
    {
      roomName: "Room Two",
      player1: "Jamal",
      judgement: "Lose",
      status: "Finished",
    },
    {
      roomName: "Room Three",
      player1: "Sugeng",
      judgement: "Draw",
      status: "Finished",
    },
    {
      roomName: "Room Four",
      player1: "Giman",
      judgement: "-",
      status: "Available",
    },
    {
      roomName: "Room Five",
      player1: "Kadi",
      judgement: "-",
      status: "Available",
    },
    {
      roomName: "Room Six",
      player1: "Bagio",
      judgement: "-",
      status: "Available",
    },
  ]);
  return (
    <div className="w-full h-screen flex">
      <div className="w-2/3 h-full bg-black p-5 pt-10">
        <Link
          to={"/vs-com"}
          className="text-black text-2xl font-bold my-5 border-4 border-yellow-500 mx-5 px-2 py-2 rounded-xl bg-slate-200 hover:transition duration-300 ease-in-out hover:bg-yellow-400"
        >
          PLAYER VS COMPUTER
        </Link>
        <div className="border-2 my-10"></div>

        <Link
          to={"/create-room"}
          className="text-black text-2xl font-bold my-5 border-4 border-yellow-500 mx-5 px-2 py-2 rounded-xl bg-slate-200 hover:transition duration-300 ease-in-out hover:bg-yellow-400"
        >
          CREATE NEW ROOM
        </Link>

        <div className="flex flex-row flex-wrap mt-10">
          {rooms.map((room) => {
            return (
              <div className="w-1/4 border border-black rounded-xl mx-10 my-5 bg-slate-300 hover:scale-110 hover:cursor-pointer transition-all">
                <Link to={"/join-room"}>
                  <div className="font-bold mb-2 py-4 text-2xl border-b rounded-xl border-black text-center bg-slate-600 text-white">
                    {room.roomName}
                  </div>
                  <div className="flex flex-col font-semibold  px-5 text-lg">
                    <div className="my-1">{`Winner : ${room.judgement}`}</div>
                    <div className="my-1">{`Room Master : ${room.player1}`}</div>
                    <div className="my-1 pb-3">{`Status Room : ${room.status}`}</div>
                  </div>
                </Link>
              </div>
            );
          })}
        </div>
      </div>

      <div className="w-1/3 border-8 border-yellow-500 p-8 px-10 bg-slate-300">
        <img
          src="./assets/images/rockpaperscissor.jpg"
          alt=""
          className="rounded-full hover:rounded hover:duration-300 hover:ease-in-out"
        />
        <div className="text-xl font-semibold tracking-wide mt-5 text-white">
          <FormBiodata variant="text-white" />
        </div>
        <div className="flex justify-center mt-10">
          <Button variant="bg-blue-500 text-black text-xl hover:scale-105 hover:cursor-pointer transition-all rounded">
            Update Bio
          </Button>
          <Button variant="bg-blue-500 text-black text-xl ml-5 hover:scale-105 hover:cursor-pointer transition-all rounded">
            <Link to={"/history"}>Game History</Link>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AllRoom;
